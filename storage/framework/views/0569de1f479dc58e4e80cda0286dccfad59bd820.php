<?php if(isset($beforeOut)): ?> <?php echo e($beforeOut); ?> <?php endif; ?>
<div class="card card-media" data-code="<?php echo e($media->shortCode); ?>">
    <?php if(isset($before)): ?> <?php echo e($before); ?> <?php endif; ?>
    <div class="card-image">
        <div class="img-box">
            <img src="<?php echo e($media->imageHighResolutionUrl); ?>">
        </div>
    </div>
    <div class="card-content">
        <div class="row">
            <div class="col s6">
                <i class="tiny material-icons">comment</i>
                <span class="comments-count"><?php echo e($media->commentsCountFormated); ?></span>
            </div>
            <div class="col s6">
                <i class="tiny material-icons">favorite</i>
                <span class="likes-count"><?php echo e($media->likesCountFormated); ?></span>
            </div>
        </div>
    </div>
    <?php if(isset($after)): ?> <?php echo e($after); ?> <?php endif; ?>
</div>
