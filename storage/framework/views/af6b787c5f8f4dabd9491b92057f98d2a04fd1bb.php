<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SortIG - Baixar comentários</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="assets/less/app.less" />
</head>
<body class="full-page">

    <div class="container download-page">
        <h1>Baixar comentários</h1>

        <div class="row">
            <div class="col s12 m4">
                <p>
                    <label>
                        <input type="checkbox" id="notify" class="filled-in" />
                        <span>Notificar quando finalizar</span>
                    </label>
                </p>
            </div>
            <div class="col s12 m4">
                <span class="percent">100%</span>
            </div>
            <div class="col s12 m4">
                <div class="card card-media" data-code="">
                    <div class="card-image">
                        <div class="img-box">
                            <img src="">
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col s6">
                                <i class="tiny material-icons">comment</i>
                                <span class="comments-count"></span>
                            </div>
                            <div class="col s6">
                                <i class="tiny material-icons">favorite</i>
                                <span class="likes-count"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>var base_url = '<?php echo e(url('/')); ?>';</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
    <script src=" https://zeptojs.com/zepto.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>

    <script>
        var currentUser = { username: '<?php echo e(session('user')->getUsername()); ?>', profile_pic_url:  '<?php echo e(session('user')->getProfilePicUrl()); ?>' };
        var OneSignal = window.OneSignal || [];
        OneSignal.push(function() {
            OneSignal.init({
                appId: "5e5a822c-b522-4c6b-8bf8-09de56d88df9",
                autoRegister: false,
                notifyButton: {
                    enable: false
                },
                allowLocalhostAsSecureOrigin: true,
            });
        });

        OneSignal.push(function() {
            OneSignal.on('notificationPermissionChange', function(permissionChange) {
                var currentPermission = permissionChange.to;
                if (currentPermission !== 'granted')
                    $('#notify').prop('checked', false);
            });
        });

        $('#notify').change(function() {
            if ($(this).is(':checked')) {
                OneSignal.registerForPushNotifications();
            }
        });
    </script>

    <script src="assets/js/sort.js"></script>

</body>
</html>
