<nav id="navbar">
    <div class="container">
        <div class="nav-wrapper">
            <a href="<?php echo e(url('/')); ?>" class="brand-logo"><?php echo e(config('app.name')); ?></a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="sass.html">Política de privacidade</a></li>
                <?php if(isset($navLinks)): ?>
                    <?php echo $navLinks; ?>

                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <li><a href="sass.html">Política de privacidade</a></li>
    <?php if(isset($navLinks)): ?>
        <?php echo $navLinks; ?>

    <?php endif; ?>
</ul>