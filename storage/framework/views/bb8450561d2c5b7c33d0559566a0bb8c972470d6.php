<?php $__env->startSection('title', 'Selecione a imagem!'); ?>
<?php $__env->startSection('body-class', 'full-page'); ?>

<?php $__env->startSection('content'); ?>

    <?php $__env->startComponent('components.header'); ?>
        <?php $__env->slot('links'); ?>
            <li><a href="#user-data-modal" class="modal-trigger">Novo sorteio</a></li>
        <?php $__env->endSlot(); ?>
    <?php echo $__env->renderComponent(); ?>

    <div class="container">
        <h1>Escolha uma postagem</h1>
        <form method="post" action="" id="form-search-media">
            <div class="row">
                <div class="col s12 m9">
                    <div class="input-field input-white">
                        <input id="url-input" type="url" class="validate" name="email" required>
                        <label for="url-input">Link da postagem no Instagram</label>
                    </div>
                </div>
                <div class="col s12 m3">
                    <button class="btn white blue-text text-darken-2 btn-large">Pesquisar</button>
                </div>
            </div>
        </form>

        <h4>Ou escolha uma postagem de <strong><?php echo e('@'.session('user')->getUsername()); ?></strong></h4>
        <div class="row media-list">
            <?php $__currentLoopData = $medias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $media): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col s6 m3">
                    <?php $__env->startComponent('components.card_media', ['media' => $media]); ?>
                    <?php echo $__env->renderComponent(); ?>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>

    <div id="search-result-modal" class="modal">
        <div class="modal-content">
            <?php $__env->startComponent('components.card_media', ['media' => new \App\Backend\Instagram\Model\Media]); ?>
                <?php $__env->slot('before'); ?>
                    <div class="chip">
                        <img src="">
                        <span></span>
                    </div>
                <?php $__env->endSlot(); ?>
                <?php $__env->slot('after'); ?>
                    <div class="card-action">
                        <a href="javascript:;" id="select-search-modal">Selecionar</a>
                        <a href="javascript:;" id="cancel-search-modal">Cancelar</a>
                    </div>
                <?php $__env->endSlot(); ?>
            <?php echo $__env->renderComponent(); ?>
        </div>
    </div>

    <?php echo $__env->make('partials.spinner', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('partials.login_modal', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <form method="post" action="<?php echo e(route('sort.store')); ?>" id="form-code">
        <?php echo e(csrf_field()); ?>

        <?php echo e(method_field('put')); ?>

        <input type="hidden" name="code" id="code-input">
    </form>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

    <script>
        var currentUser = { username: '<?php echo e(session('user')->getUsername()); ?>', profile_pic_url:  '<?php echo e(session('user')->getProfilePicUrl()); ?>' },
            hasMediaEnd = <?php echo e(!$hasNext ? 'true' : 'false'); ?>,
            mediaMaxId = <?php echo $hasNext ? '"' . $maxId . '"' : 'null'; ?>;
    </script>
    <script src="<?php echo e(asset('assets/js/sort.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>