<div id="user-data-modal" class="modal">
    <div class="modal-content">
        <img src="{{ asset('assets/img/anonymous-avatar.jpg') }}" alt="" class="circle image-user">
        <div class="row">
            <form class="col s12" id="form-user" method="post" action="{{route('sort.start')}}">
                {{csrf_field()}}
                {{method_field('post')}}
                <div class="row">
                    <div class="input-field col s12">
                        <span class="prefix">@</span>
                        <input id="username-input" type="text" class="validate" name="username">
                        <label for="username-input">Usuário</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="email-input" type="email" class="validate" name="email">
                        <label for="email-input">Email</label>
                    </div>
                    <div class="input-field col s12 btn-col">
                        <button class="btn">Próximo</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>