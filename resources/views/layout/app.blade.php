<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}" />
    @yield('stylesheets')
</head>
<body class="@yield('body-class')">

    @yield('content')

    <script>var base_url = '{{url('/')}}';</script>
    <script src="//zeptojs.com/zepto.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
    @yield('scripts')
    @if ($errors->any())
        <script>
            toastError('{{$errors->first('message')}}');
        </script>
    @endif

</body>
</html>
