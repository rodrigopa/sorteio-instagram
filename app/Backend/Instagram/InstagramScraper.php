<?php

namespace App\Backend\Instagram;

use InstagramScraper\Endpoints;
use InstagramScraper\Exception\InstagramException;
use InstagramScraper\Exception\InstagramNotFoundException;
use InstagramScraper\Model\Comment;
use Unirest\Request;

class InstagramScraper extends \InstagramScraper\Instagram
{

    protected $userSession = [];
    protected $rhxGis = null;

    const COMMENTS_BEFORE_COMMENT_ID_BY_CODE = 'https://www.instagram.com/graphql/query/?query_hash=f0986789a5c5d17c2400faebf16efd0d&variables={variables}';

    public function getMediaCommentsByCode($code, $count = self::MAX_COMMENTS_PER_REQUEST, $maxId = null)
    {
        if (is_null($maxId))
            $maxId = '';

        $variables = json_encode([
            'shortcode' => (string)$code,
            'first' => (string)$count,
            'after' => (string)$maxId
        ]);
        $comments = [];
        $commentsUrl = self::getCommentsBeforeCommentIdByCode($variables);
        $response = Request::get($commentsUrl, $this->generateHeaders($this->userSession, $this->generateGisToken($variables)));

        if (static::HTTP_OK !== $response->code) {
            throw new InstagramException('Response code is ' . $response->code . '. Body: ' . static::getErrorBody($response->body) . ' Something went wrong. Please report issue.', $response->code);
        }
        $cookies = self::parseCookies($response->headers['Set-Cookie']);
        $this->userSession['csrftoken'] = $cookies['csrftoken'];
        $jsonResponse = $this->decodeRawBodyToJson($response->raw_body);
        $nodes = $jsonResponse['data']['shortcode_media']['edge_media_to_comment']['edges'];
        foreach ($nodes as $commentArray) {
            $comments[] = Comment::create($commentArray['node']);
        }
        $hasNext = $jsonResponse['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page'];
        $maxId = $jsonResponse['data']['shortcode_media']['edge_media_to_comment']['page_info']['end_cursor'];

        return [
            'comments' => $comments,
            'hasNext' => $hasNext,
            'maxId' => $maxId
        ];
    }

    public static function getCommentsBeforeCommentIdByCode($variables)
    {
        return str_replace('{variables}', urlencode($variables), self::COMMENTS_BEFORE_COMMENT_ID_BY_CODE);
    }

    /**
     * @param string $url
     * @return mixed|null
     * @throws InstagramException
     * @throws InstagramNotFoundException
     */
    private function getSharedDataFromPage($url = Endpoints::BASE_URL)
    {
        $response = Request::get(rtrim($url, '/') . '/', $this->generateHeaders($this->userSession));
        if (static::HTTP_NOT_FOUND === $response->code) {
            throw new InstagramNotFoundException("Page {$url} not found");
        }

        if (static::HTTP_OK !== $response->code) {
            throw new InstagramException('Response code is ' . $response->code . '. Body: ' . static::getErrorBody($response->body) . ' Something went wrong. Please report issue.', $response->code);
        }

        return self::extractSharedDataFromBody($response->raw_body);
    }

    private static function extractSharedDataFromBody($body)
    {
        if (preg_match_all('#\_sharedData \= (.*?)\;\<\/script\>#', $body, $out)) {
            return json_decode($out[1][0], true, 512, JSON_BIGINT_AS_STRING);
        }
        return null;
    }


    /**
     * @param \stdClass|string $rawError
     *
     * @return string
     */
    private static function getErrorBody($rawError)
    {
        if (is_string($rawError)) {
            return $rawError;
        }
        if (is_object($rawError)) {
            $str = '';
            foreach ($rawError as $key => $value) {
                $str .= ' ' . $key . ' => ' . $value . ';';
            }
            return $str;
        } else {
            return 'Unknown body format';
        }

    }

    /**
     * @return null
     * @throws InstagramException
     */
    private function getRhxGis()
    {
        if ($this->rhxGis === null) {
            try {
                $sharedData = $this->getSharedDataFromPage();
                $this->rhxGis = $sharedData['rhx_gis'];
            } catch (\Exception $exception) {
                throw new InstagramException('Could not extract gis from page');
            }
        }

        return $this->rhxGis;
    }

    /**
     * @param $variables
     * @return string
     * @throws InstagramException
     */
    private function generateGisToken($variables)
    {
        return md5(implode(':', [$this->getRhxGis(), $variables]));
    }

    /**
     * @param $session
     * @param $gisToken
     *
     * @return array
     */
    private function generateHeaders($session, $gisToken = null)
    {
        $headers = [];
        if ($session) {
            $cookies = '';
            foreach ($session as $key => $value) {
                $cookies .= "$key=$value; ";
            }
            $headers = [
                'cookie' => $cookies,
                'referer' => Endpoints::BASE_URL . '/',
                'x-csrftoken' => $session['csrftoken'],
            ];
        }

        if ($this->getUserAgent()) {
            $headers['user-agent'] = $this->getUserAgent();

            if (!is_null($gisToken)) {
                $headers['x-instagram-gis'] = $gisToken;
            }
        }

        return $headers;
    }


    /**
     * We work only on https in this case if we have same cookies on Secure and not - we will choice Secure cookie
     *
     * @param string $rawCookies
     *
     * @return array
     */
    private static function parseCookies($rawCookies)
    {
        if (!is_array($rawCookies)) {
            $rawCookies = [$rawCookies];
        }

        $not_secure_cookies = [];
        $secure_cookies = [];

        foreach ($rawCookies as $cookie) {
            $cookie_array = 'not_secure_cookies';
            $cookie_parts = explode(';', $cookie);
            foreach ($cookie_parts as $cookie_part) {
                if (trim($cookie_part) == 'Secure') {
                    $cookie_array = 'secure_cookies';
                    break;
                }
            }
            $value = array_shift($cookie_parts);
            $parts = explode('=', $value);
            if (sizeof($parts) >= 2 && !is_null($parts[1])) {
                ${$cookie_array}[$parts[0]] = $parts[1];
            }
        }

        $cookies = $secure_cookies + $not_secure_cookies;
        return $cookies;
    }

    /**
     * @param $rawBody
     * @return mixed
     */
    private function decodeRawBodyToJson($rawBody)
    {
        return json_decode($rawBody, true, 512, JSON_BIGINT_AS_STRING);
    }

}