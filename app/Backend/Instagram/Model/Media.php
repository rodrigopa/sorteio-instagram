<?php

namespace App\Backend\Instagram\Model;

use App\Helpers\InstagramHelper;

class Media extends BaseModel
{
    public $id;
    public $user;
    public $shortCode;
    public $likesCount;
    public $commentsCount;
    public $likesCountFormated;
    public $commentsCountFormated;
    public $imageHighResolutionUrl;

    public function __construct($data = null)
    {
        if ($data instanceof \InstagramScraper\Model\Media) {
            $this->id = $data->getId();
            $this->user = new User($data->getOwner());
            $this->shortCode = $data->getShortCode();
            $this->likesCount = $data->getLikesCount();
            $this->likesCountFormated = $this->getLikesCountFormated();
            $this->commentsCount = $data->getCommentsCount();
            $this->commentsCountFormated = $this->getCommentsCountFormated();
            $this->imageHighResolutionUrl = $data->getImageHighResolutionUrl();
        } else if (is_array($data)) {
            $this->set_object_vars($this, $data);
            $this->user = new User($data['user']);
        }
    }

    public function getLikesCountFormated()
    {
        return InstagramHelper::coolNumberFormat($this->likesCount);
    }

    public function getCommentsCountFormated()
    {
        return InstagramHelper::coolNumberFormat($this->commentsCount);
    }

}
