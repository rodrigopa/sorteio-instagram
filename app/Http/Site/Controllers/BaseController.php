<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 04/10/18
 * Time: 10:56
 */

namespace App\Http\Site\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}