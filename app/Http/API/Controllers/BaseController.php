<?php
/**
 * Created by PhpStorm.
 * User: rodrigo
 * Date: 04/10/18
 * Time: 10:56
 */

namespace App\Http\API\Controllers;

use App\Backend\Exceptions\PrivateUserException;
use App\Backend\Instagram\Instagram;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    const HTTP_STATUS_OK = 200;
    const HTTP_STATUS_BAD = 400;

    public function json($data, $error = false, $variables = null)
    {
        $status = ($error ? self::HTTP_STATUS_BAD : self::HTTP_STATUS_OK);

        if ($error)
            $data = ['message' => $data];
        else
            $data = ['data' => $data];

        $data['error'] = $error;

        if (is_array($variables))
            $data['data'] = $variables;

        return response()
            ->json($data, $status);
    }

}