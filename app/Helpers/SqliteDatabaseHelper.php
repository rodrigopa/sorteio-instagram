<?php

namespace App\Helpers;

use PDO;

class SqliteDatabaseHelper
{
    public $db;
    public $path;
    public $code;

    public function __construct($code, $createFile = false)
    {
        $this->code = $code;
        $this->path = storage_path('app/db/' . $code . '.sqlite');
        if ($createFile)
            $this->createFile();
        $this->db = $this->createPdoConnection();
    }

    private function createFile()
    {
        //        dd($path);
//        if (!file_exists($this->path))
//            mkdir(dirname($this->path), 0775, true);

        file_put_contents($this->path, '');
    }

    private function createPdoConnection()
    {
        $pdo = new PDO('sqlite:' . $this->path);
        $pdo->setAttribute(PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }
}