<?php

namespace App\Helpers;

class InstagramHelper
{
    public static function coolNumberFormat($number)
    {
        if ($number === 0) return 0;
        $abbrevs = array(12 => "T", 9 => "B", 6 => "M", 3 => "K", 0 => "");
        foreach($abbrevs as $exponent => $abbrev) {
            if($number >= pow(10, $exponent)) {
                $display_num = $number / pow(10, $exponent);
                $decimals = ($exponent >= 3 && round($display_num) < 100) ? 1 : 0;
                return number_format($display_num,$decimals) . $abbrev;
            }
        }
    }
}