<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Sort;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

Route::get('/', function () {
    return view('index');
});

Route::get('/routes', function () {

    dd(Route::getRoutes());

});


Route::group(['namespace' => '\App\Http\Site\Controllers'], function () {

    Route::post('sort/start', 'SortController@start')->name('sort.start');

    Route::group(['middleware' => ['started_session']], function() {
        Route::put('sort', 'SortController@store')->name('sort.store');
        Route::get('sort/{id}', 'SortController@show')->name('sort.show');
        Route::get('sort/{id}/status', 'SortController@status')->name('sort.status');
        Route::post('sort/{id}/updateNotify', 'SortController@updateNotify')->name('sort.updateNotify');
    });

    Route::get('sort/{id}/generate', 'SortController@generateForm')->name('sort.generateForm');
    Route::post('sort/{id}/generated', 'SortController@generate')->name('sort.generate');

});
