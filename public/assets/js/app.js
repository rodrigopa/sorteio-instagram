var elems = document.querySelectorAll('.modal');
var instances = M.Modal.init(elems);

var toastError = function(msg)
{
    M.toast({html: msg, classes: 'red darken-2', displayLength: 6e3 })
};

M.Sidenav.init(document.querySelectorAll('.sidenav'));

$.ajaxSettings = $.extend($.ajaxSettings, {
    async: true,
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * LOGIN
 */
var userLoginError = false,
    userLoginErrorMessage;

$('#username-input').blur(function() {

    var username = $.trim( $(this).val() );

    if (!username) return;

    $.ajax({
        type: 'get',
        dataType: 'json',
        url: base_url + '/api/social/user/profile-picture',
        data: { username: username },
        error: function(request) {
            var jsonResponse = $.parseJSON(request.response);
            userLoginError = true;
            userLoginErrorMessage =jsonResponse.message;
            toastError(jsonResponse.message);
            $('#username-input').removeClass('valid invalid').addClass('invalid');
        },
        success: function(response) {
            userLoginError = false;
            $('.image-user').prop('src', response.data.url);
            $('#username-input').removeClass('valid invalid').addClass('valid');
        }
    });

});

$('#form-user').submit(function() {

    var username = $.trim( $(this).find('#username-input').val() );
    var email = $.trim( $(this).find('#email-input').val() );

    if (!username || !email) {
        toastError('Digite seu usuário e seu email!');
        return false;
    }

    if (userLoginError) {
        toastError(userLoginErrorMessage);
        return false;
    }

    return true;

});
