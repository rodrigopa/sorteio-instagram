var loadingMoreMedia = false;
var getMediaCardTemplate = function() {
    var clonedTemplate = $('.media-list > .col:first-child').clone().wrap("<div />").parent();
    clonedTemplate.find('.img-box img').prop('src', '{{thumbnail_src}}');
    clonedTemplate.find('.comments-count').text('{{comments_count}}');
    clonedTemplate.find('.likes-count').text('{{likes_count}}');

    return clonedTemplate.html();
};

$(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        if (loadingMoreMedia || hasMediaEnd) return false;

        loadingMoreMedia = true;
        $('.media-list').append( $('#spinner-loading').html() );

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: base_url + '/api/social/user/medias',
            data: { maxId: mediaMaxId, username: currentUser.username },
            error: function(request) {
                loadingMoreMedia = false;
                var jsonResponse = $.parseJSON(request.response);
                toastError(jsonResponse.message);
            },
            success: function(response) {
                if (response.data.hasNext)
                    mediaMaxId = response.data.maxId;
                else {
                    hasMediaEnd = true;
                }

                var template = getMediaCardTemplate();
                var html = '';

                $.each(response.data.media, function(index, media) {
                    console.log(media);
                    html += template.replace('{{thumbnail_src}}', media.imageHighResolutionUrl).
                        replace('{{comments_count}}', media.commentsCountFormated).
                        replace('{{likes_count}}', media.likesCountFormated);
                });

                $('.media-list').append(html);
                setTimeout(function() {
                    loadingMoreMedia = false;
                }, 200);
            },
            complete: function() {
                $('.media-list').find('.preloader-wrapper').remove();
            }
        });
    }
});

$('#username-input').blur(function() {

    var username = $.trim( $(this).val() );

    if (!username) return;

    $.ajax({
        type: 'get',
        dataType: 'json',
        url: base_url + '/api/social/user/profile-picture',
        data: { username: currentUser.username },
        error: function(request) {
            var jsonResponse = $.parseJSON(request.response);
            userLoginError = true;
            userLoginErrorMessage =jsonResponse.message;
            toastError(jsonResponse.message);
            $('#username-input').removeClass('valid invalid').addClass('invalid');
        },
        success: function(response) {
            userLoginError = false;
            $('.image-user').prop('src', response.data.url);
            $('#username-input').removeClass('valid invalid').addClass('valid');
        }
    });

});

var searchLoading = false;
var modalElem = $('#search-result-modal');

var showSelectedMediaModal = function(data) {
    var chip = modalElem.find('.chip');
    var instance = M.Modal.getInstance(modalElem[0]);

    chip.find('img').prop('src', data.user.profilePicUrl);
    chip.find('span').text(data.user.username);

    modalElem.find('.card-image img').prop('src', data.imageHighResolutionUrl);
    modalElem.find('.comments-count').text(data.commentsCount);
    modalElem.find('.likes-count').text(data.likesCount);
    modalElem.data('code', data.shortCode);

    instance.open();
};

$('#form-search-media').submit(function() {
    if (searchLoading)
        return false;

    searchLoading = true;

    var qInput = $(this).find('#url-input');
    var q = $.trim( qInput.val() );

    qInput.val('Carregando...');
    qInput.prop('disabled', true);

    $.ajax({
        type: 'get',
        dataType: 'json',
        url: base_url + '/api/social/media/search',
        data: { q: q },
        error: function(request) {
            var jsonResponse = $.parseJSON(request.response);
            toastError(jsonResponse.message);
            $('#url-input').removeClass('valid invalid').addClass('invalid');
        },
        success: function(response) {
            showSelectedMediaModal(response.data.media);
            $('#url-input').removeClass('valid invalid').addClass('valid');
        },
        complete: function() {
            searchLoading = false;
            qInput.val(q);
            qInput.prop('disabled', false);
        }
    });

    return false;

});

$('#cancel-search-modal').click(function() {
    var instance = M.Modal.getInstance(modalElem[0]);
    instance.close();
});

$('#select-search-modal').click(function() {
    var code = modalElem.data('code');

    $('#code-input').val(code);
    $('#form-code').submit();
    $('#cancel-search-modal').trigger('click');
});

$('.media-list').on('click', '.card', function() {
    var code = $(this).data('code');
    var img = $(this).find('.card-image img').prop('src');
    var commentsCount = $(this).find('.comments-count').text();
    var likesCount = $(this).find('.likes-count').text();

    var data = {
        shortCode: code,
        imageHighResolutionUrl: img,
        commentsCount: commentsCount,
        likesCount: likesCount,
        user: {
            profilePicUrl: currentUser.profile_pic_url,
            username: currentUser.username
        }
    };

    showSelectedMediaModal(data);
});
